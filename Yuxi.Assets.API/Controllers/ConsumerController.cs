﻿using Microsoft.AspNetCore.Mvc;
using Yuxi.Assets.API.Application.Models;
using Yuxi.Assets.Domain.AggregatesModel.ConsumerAggregate;
using Yuxi.Assets.Service.Services;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Yuxi.Assets.API.Controllers
{
    [Route("api/consumer")]
    public class ConsumerController : Controller
    {

        private readonly IConsumerService _consumerService;

        #region Constructor Method  

        public ConsumerController(IConsumerService consumerService)
        {
            _consumerService = consumerService;

        }
        #endregion

        #region CreateConsumer Method

        [HttpPost]
        [Route("/create-consumer")]
        public void CreateConsumer(ConsumerReceived consumer)
        {
            _consumerService.CreateConsumer(consumer.Id, consumer.Name, consumer.Application,
                consumer.ContainerRootPath, consumer.DefaultContainer);
        }

        #endregion

        #region UpdateContainerStorage Method

        [HttpPut]
        [Route("/consumers/{id}/default-storage")]
        public void UpdateContainerStorage([FromRoute(Name = "id")]  string consumerId,
                                                                     int containerType)
        {
            _consumerService.UpdateContainerStorage(containerType, consumerId);
        }

        #endregion




    }
}
