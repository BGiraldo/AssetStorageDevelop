﻿using System;
using Microsoft.AspNetCore.Mvc;
using Yuxi.Assets.Domain.AggregatesModel.AssetAggregate;
using Yuxi.Assets.API.Application.Models;
using Yuxi.Assets.Service.Services;
using System.IO;
using System.Threading.Tasks;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Yuxi.Assets.API.Controllers
{
    [Route("api/asset")]
    [Consumes("application/json", "multipart/form-data", "application/x-www-form-urlencoded")]
    public class AssetStorageController : Controller
    {

        private readonly IAssetService _assetController;

        #region constructor method

        public AssetStorageController(IAssetService assetController)
        {
            this._assetController = assetController;

        }

        #endregion

        #region Download Post Method

        [HttpPost]
        [Route("/download")]
        public async Task<Stream> Download(Guid assetId)
        {
            MemoryStream outStream = new MemoryStream();
            await _assetController.DownloadAssetToStreamAsync(assetId, outStream);
            outStream.Position = 0;
            return outStream;
        }

        #endregion

        #region Upload Post Service

        [HttpPost]
        [Route("/upload")]
        public Guid[] Upload(FileUpload filesUpload)
        {
            Guid[] idUploads = new Guid[filesUpload.Files.Length];

            for (int i = 0; i < filesUpload.Files.Length; i++)
            {
                using (var stream = filesUpload.Files[i].OpenReadStream())
                {

                    Metadata metadata = new Metadata(filesUpload.Files[i].ContentType,
                        (int)filesUpload.Files[i].Length,
                        filesUpload.Files[i].FileName,
                        filesUpload.ConsumerId,
                        filesUpload.RelativePath);

                    Asset assetResult = _assetController.CreateAssetAsync(stream, metadata,
                                         filesUpload.ConsumerId).Result;

                    idUploads[i] = assetResult.Id;
                }
            }
            return idUploads;
        }

        #endregion

    }


}