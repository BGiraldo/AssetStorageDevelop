﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yuxi.Assets.Domain.AggregatesModel.Shared;

namespace Yuxi.Assets.API.Application.Models
{
    public class ConsumerReceived
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Application { get; set; }

        public string ContainerRootPath { get; set; }

        public int DefaultContainer { get; set; }

    }
}
