﻿using Microsoft.AspNetCore.Http;

namespace Yuxi.Assets.API.Application.Models
{
    public class FileUpload
    {

        public IFormFile[] Files { get; set; }

        public string ConsumerId { get; set; }

        public string RelativePath { get; set; }
    }
}
