﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Yuxi.Assets.Domain.AggregatesModel.ConsumerAggregate;
using Yuxi.Assets.Domain.AggregatesModel.Shared;
using Yuxi.Assets.Infrastructure;

namespace Yuxi.Assets.API.Infrastructure
{
    public class OrderingContextSeed
    {

        public static async Task SeedAsync(IApplicationBuilder applicationBuilder)
        {

            var context = (AssetStorageContext)applicationBuilder
                .ApplicationServices.GetService(typeof(AssetStorageContext));

            using (context)
            {

                context.Database.Migrate();

                if (!context.ContainerTypes.Any())
                {
                    context.ContainerTypes.Add(ContainerType.Azure);
                    context.ContainerTypes.Add(ContainerType.Disk);

                    await context.SaveChangesAsync();
                }

                if (!context.Consumers.Any())
                {
                    var containerAzure = context.ContainerTypes.Find(ContainerType.Azure.Id);
                    var containerDisk = context.ContainerTypes.Find(ContainerType.Disk.Id);

                    Consumer consumerTest = new Consumer("1", "Pablo", "web app",
                        "iugo", containerAzure);

                    Consumer consumerTest2 = new Consumer("2", "Maria", "web app",
                        "iugo", containerDisk);

                    context.Consumers.Add(consumerTest);
                    context.Consumers.Add(consumerTest2);

                    await context.SaveChangesAsync();
                }


                if (!context.ConsumerStoragePreferences.Any())
                {

                    var consumer = context.Consumers.Find("1");

                    var containerDisk = context.ContainerTypes.Find(ContainerType.Disk.Id);

                    var mimeType = "image/png";

                    ConsumerStoragePreference csp = new ConsumerStoragePreference(consumer,
                        mimeType, containerDisk);

                    context.ConsumerStoragePreferences.Add(csp);
                    await context.SaveChangesAsync();
                }

            }

        }
    }

}
