﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Yuxi.Assets.Domain.AggregatesModel.AssetAggregate;
using Yuxi.Assets.Service.Factory.AssetFactory;
using Yuxi.Assets.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Yuxi.Assets.Infrastructure.Repository;
using Yuxi.Assets.Service.Services;
using Yuxi.Assets.Domain.AggregatesModel.ConsumerAggregate;
using Yuxi.Assets.Infrastructure.Repositories;
using Yuxi.Assets.API.Infrastructure;
using Yuxi.Assets.Domain.AggregatesModel.Shared;
using Yuxi.Assets.Service.ConfigurableVariables;

namespace AssetStorage
{
    public class Startup
    {

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMvc();

            services.AddTransient<IServiceTypeFactory, AssetServiceFactory>();

            services.AddTransient<IAssetService, AssetService>();

            services.AddTransient<IConsumerService, ConsumerService>();

            services.AddTransient<IAppVariable, AppVariable>((_) => new AppVariable(Configuration));

            services.AddDbContext<AssetStorageContext>(options =>
                   options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<IAssetRepository, AssetRepository>();

            services.AddScoped<IConsumerRepository, ConsumerRepository>();

            services.AddScoped<IConsumerStoragePreferenceRepository, ConsumerStoragePreferenceRepository>();

        }





        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public async void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMvc();

            await OrderingContextSeed.SeedAsync(app);

        }
    }
}
