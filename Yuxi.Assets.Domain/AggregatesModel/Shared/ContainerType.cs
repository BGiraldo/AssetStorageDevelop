﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Yuxi.Assets.Domain.SeedWork;

namespace Yuxi.Assets.Domain.AggregatesModel.Shared
{
    public class ContainerType : Enumeration
    {
        public static ContainerType Disk = new ContainerType(0, "Disk");
        public static ContainerType Azure = new ContainerType(1, "Azure");

        protected ContainerType() { }

        public ContainerType(int id, string name)
            : base(id, name)
        {

        }

        public static IEnumerable<ContainerType> List()
        {
            return new[] { Disk, Azure };
        }

        public static ContainerType FromName(string name)
        {
            var state = List()
                .SingleOrDefault(s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));

            if (state == null)
            {
                throw new ArgumentException($"Possible values for ContainerType: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }

        public static ContainerType From(int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);

            if (state == null)
            {
                throw new ArgumentException($"Possible values for ContainerType: {String.Join(",", List().Select(s => s.Name))}");
            }

            return state;
        }

    }
}
