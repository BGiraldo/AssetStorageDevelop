﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yuxi.Assets.Domain.AggregatesModel.Shared
{
   public interface IAppVariable
    {
        string StorageAccountName { get; }

        string AzureKey { get; }

        string ContainerName { get; }

        string DiskRootPath { get; }
        

    }
}
