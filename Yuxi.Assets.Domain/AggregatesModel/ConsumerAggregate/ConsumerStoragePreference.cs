﻿using System;
using System.Collections.Generic;
using System.Text;
using Yuxi.Assets.Domain.AggregatesModel.Shared;
using Yuxi.Assets.Domain.SeedWork;


namespace Yuxi.Assets.Domain.AggregatesModel.ConsumerAggregate
{
    public class ConsumerStoragePreference : IAggregateRoot
    {

        public Guid Id { get; private set; }

        public Consumer Consumer { get; private set; }

        public string MimeType { get; private set; }

        public ContainerType Container { get; private set; }

        public ConsumerStoragePreference(Consumer consumer, string mimeType, ContainerType container)
        {
            Id = Guid.NewGuid();
            Consumer = consumer;
            MimeType = mimeType;
            Container = container;
        }

        public ConsumerStoragePreference() { }


    }
}
