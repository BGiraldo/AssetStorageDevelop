﻿using System.Threading.Tasks;
using Yuxi.Assets.Domain.SeedWork;

namespace Yuxi.Assets.Domain.AggregatesModel.ConsumerAggregate
{
  public interface IConsumerStoragePreferenceRepository : IRepository<ConsumerStoragePreference> {

        Task<ConsumerStoragePreference> ObtainConsumerStoragePreferences(string consumerId);


    }
}
