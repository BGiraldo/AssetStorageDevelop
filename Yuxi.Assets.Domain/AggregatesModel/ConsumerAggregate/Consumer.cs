﻿using System;
using System.Collections.Generic;
using System.Text;
using Yuxi.Assets.Domain.AggregatesModel.Shared;
using Yuxi.Assets.Domain.SeedWork;


namespace Yuxi.Assets.Domain.AggregatesModel.ConsumerAggregate
{
    public class Consumer : IAggregateRoot
    {

        public string Id { get; private set; }

        public string Name { get; private set; }

        public string Application { get; private set; }

        public string ContainerRootPath { get; private set; }

        public ContainerType DefaultContainer { get; set; }

        public Consumer(string id, string name, string application, string containerRootPath, ContainerType defaultContainer)
        {
            Id = id;
            Name = name;
            Application = application;
            ContainerRootPath = containerRootPath;
            DefaultContainer = defaultContainer;
        }

        public Consumer() { }

    }
}
