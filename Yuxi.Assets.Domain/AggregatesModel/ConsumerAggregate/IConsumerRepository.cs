﻿using System.Threading.Tasks;
using Yuxi.Assets.Domain.AggregatesModel.Shared;
using Yuxi.Assets.Domain.SeedWork;

namespace Yuxi.Assets.Domain.AggregatesModel.ConsumerAggregate
{
   public interface IConsumerRepository : IRepository<Consumer> {

        Task<Consumer> FindAsync(string idObject);

        void UpdateContainerStorage(int containerTypeId, string consumerId);

        ContainerType ObtainerContainerType(int defaultContainer);

    }
}
