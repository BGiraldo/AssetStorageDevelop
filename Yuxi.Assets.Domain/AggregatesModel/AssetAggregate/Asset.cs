﻿using System;
using Yuxi.Assets.Domain.AggregatesModel.ConsumerAggregate;
using Yuxi.Assets.Domain.AggregatesModel.Shared;
using Yuxi.Assets.Domain.SeedWork;

namespace Yuxi.Assets.Domain.AggregatesModel.AssetAggregate
{
    public class Asset : IAggregateRoot
    {

        public Guid Id { get; private set; }

        public Consumer Consumer { get; private set; }

        public Metadata Metadata { get; private set; }

        public ContainerType ContainerType { get; private set; }

        public DateTime UpdatedDate { get; private set; }

        public Asset(Consumer consumer, Metadata metadata, ContainerType containerType, DateTime updatedDate)
        {
            Id = Guid.NewGuid();
            Consumer = consumer;
            Metadata = metadata;
            ContainerType = containerType;
            UpdatedDate = updatedDate;
        }

        public Asset() { }


    }
}
