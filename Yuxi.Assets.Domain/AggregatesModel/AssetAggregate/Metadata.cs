﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yuxi.Assets.Domain.AggregatesModel.AssetAggregate
{
    public class Metadata
    {

        public Guid Id { get; private set; }

        public string MimeType { get; private set; }

        public int SizeKb { get; private set; }

        public string Name { get; private set; }

        public string ContainerRootPath { get; set; }

        public string RelativePath { get; private set; }


        public Metadata(string mimeType, int sizeKb, string name, string containerRootPath, string relativePath)
        {
            Id = Guid.NewGuid();
            MimeType = mimeType;
            SizeKb = sizeKb;
            Name = name;
            ContainerRootPath = "YuxiAssets_"+containerRootPath;
            RelativePath = relativePath;
        }

        public Metadata() { }

    }
}
