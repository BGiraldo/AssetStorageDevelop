﻿using System;
using System.Collections.Generic;
using Yuxi.Assets.Domain.SeedWork;

namespace Yuxi.Assets.Domain.AggregatesModel.AssetAggregate
{
    public interface IAssetRepository : IRepository<Asset>
    {

        void Remove(Guid AssetId);

        List<Asset> List(string path);

    }

}
