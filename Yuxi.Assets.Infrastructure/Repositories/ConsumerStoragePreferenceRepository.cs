﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yuxi.Assets.Domain.AggregatesModel.ConsumerAggregate;

namespace Yuxi.Assets.Infrastructure.Repositories
{
    public class ConsumerStoragePreferenceRepository : IConsumerStoragePreferenceRepository
    {

        private readonly AssetStorageContext _context;

        public ConsumerStoragePreferenceRepository(AssetStorageContext context)
        {
            _context = context;
        }

        public void Add(ConsumerStoragePreference consumerStorage)
        {
            _context.ConsumerStoragePreferences.Add(consumerStorage);
            _context.SaveChanges();
        }

        public async Task<ConsumerStoragePreference> FindAsync(Guid consumerStorageId)
        {
            return await _context.ConsumerStoragePreferences
                .Include(csp => csp.Consumer)
                .Include(csp => csp.Container)
               .FirstOrDefaultAsync(csp => csp.Id == consumerStorageId);
        }

        public async Task<ConsumerStoragePreference> ObtainConsumerStoragePreferences(string consumerId)
        {
   
                return await _context.ConsumerStoragePreferences
                               .Include(csp => csp.Consumer)
                               .Include(csp => csp.Container)
                              .FirstOrDefaultAsync(csp => csp.Consumer.Id == consumerId);
        }

        public void Update(ConsumerStoragePreference consumerStorage)
        {
            _context.ConsumerStoragePreferences.Update(consumerStorage);
            _context.SaveChanges();
        }
    }
}
