﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yuxi.Assets.Domain.AggregatesModel.ConsumerAggregate;
using Yuxi.Assets.Domain.AggregatesModel.Shared;

namespace Yuxi.Assets.Infrastructure.Repositories
{
    public class ConsumerRepository : IConsumerRepository
    {

        private readonly AssetStorageContext _context;

        public ConsumerRepository(AssetStorageContext context)
        {
            _context = context;
        }

        public void Add(Consumer consumer)
        {
            _context.Consumers.Add(consumer);
            _context.SaveChanges();
        }

        public async Task<Consumer> FindAsync(Guid consumerId)
        {
            return await _context.Consumers
                .Include(consumer => consumer.DefaultContainer)
               .FirstOrDefaultAsync(consumer => consumer.Id == consumerId.ToString());
        }

        public void Update(Consumer consumer)
        {
            _context.Consumers.Update(consumer);
            _context.SaveChanges();
        }

        public async Task<Consumer> FindAsync(string consumerId)
        {
            return await _context.Consumers
                .Include(consumer => consumer.DefaultContainer)
                .FirstOrDefaultAsync(consumer => consumer.Id == consumerId);
        }

        public void UpdateContainerStorage(int containerTypeId, string consumerId)
        {
            Consumer consumer = FindAsync(consumerId).Result;
            ContainerType containerType = _context.ContainerTypes.FindAsync(containerTypeId).Result;
            consumer.DefaultContainer = containerType;
            _context.Consumers.Update(consumer);
            _context.SaveChanges();
        }

        public ContainerType ObtainerContainerType(int defaultContainer)
        {
            return _context.ContainerTypes.FindAsync(defaultContainer).Result;
        }

    }
}
