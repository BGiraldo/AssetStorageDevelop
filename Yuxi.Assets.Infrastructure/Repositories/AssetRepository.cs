﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yuxi.Assets.Domain.AggregatesModel.AssetAggregate;



namespace Yuxi.Assets.Infrastructure.Repository
{
    public class AssetRepository : IAssetRepository
    {

        private readonly AssetStorageContext _context;

        public AssetRepository(AssetStorageContext context)
        {
            _context = context;
        }

        public void Add(Asset asset)
        {
            _context.Assets.Add(asset);
            _context.SaveChanges();
        }

        public async Task<Asset> FindAsync(Guid assetId)
        {
            return await _context.Assets
                  .Include(asset => asset.Consumer)
                     .ThenInclude(consumer => consumer.DefaultContainer)
                  .Include(asset => asset.Metadata)
                  .Include(asset => asset.ContainerType)
                  .FirstOrDefaultAsync(asset => asset.Id == assetId);
        }

        public void Update(Asset asset)
        {
            _context.Assets.Update(asset);
            _context.SaveChanges();
        }

        public void Remove(Guid AssetId)
        {
            var asset = _context.Assets.FindAsync(AssetId).Result;
            _context.Assets.Remove(asset);
            _context.SaveChanges();
        }

        public List<Asset> List(string path)
        {
            List<Asset> assetsInBd = new List<Asset>();

            foreach (var asset in _context.Assets)
            {
                assetsInBd.Add(asset);
            }
            return assetsInBd;
        }
    }
}
