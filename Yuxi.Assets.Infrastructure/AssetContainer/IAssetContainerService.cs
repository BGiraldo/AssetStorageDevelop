﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Yuxi.Assets.Domain.AggregatesModel.AssetAggregate;
using Yuxi.Assets.Domain.AggregatesModel.Shared;

namespace Yuxi.Assets.Infrastructure.AssetContainer
{
    public interface IAssetContainerService
    {

        Asset CreateAsset(Stream inStream, Metadata metadata, string storagePath);

        Asset LoadAsset(Guid assetId);

        void DeleteAsset(Guid assetId);

        Asset OverwritteAsset(Stream inStream, Metadata metadata, string storagePath, Guid assetId);

        List<Asset> ListAssets(string storagePath);

        Task DownloadToStreamAsync(Stream inStream);

        Task SaveAssetAsync(Stream inStream);



    }
}
