﻿using System;
using System.Threading.Tasks;
using System.IO;
using Yuxi.Assets.Domain.AggregatesModel.AssetAggregate;
using System.Collections.Generic;
using Yuxi.Assets.Infrastructure.AssetContainer;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Yuxi.Assets.Domain.AggregatesModel.Shared;

namespace Yuxi.Assets.Service.Infrastructure.AssetContainer
{
    public class AzureAssetContainer : IAssetContainerService
    {
        #region attributes

        private readonly string StorageAccountName;

        private readonly string AzureKey;

        private readonly string ContainerName;

        private readonly ICloudBlob blobStorage;

        #endregion

        #region constructor 

        public AzureAssetContainer(string storagePath, string storageAccountName, string azureKey, string containerName)
        {
            StorageAccountName = storageAccountName;
            AzureKey = azureKey;
            ContainerName = containerName;

            var cloudBlobClient = ObtainAzureConnection(StorageAccountName, AzureKey);
            var container = ObtainContainer(ContainerName, cloudBlobClient).Result;

            this.blobStorage = container.GetBlockBlobReference(storagePath);
        }

        #endregion

        #region CreateAsset Method

        public Asset CreateAsset(Stream inStream, Metadata metadata, string path)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region SaveAssetAsync Method

        public async Task SaveAssetAsync(Stream inStream)
        {
            await blobStorage.UploadFromStreamAsync(inStream);
        }

        #endregion

        #region ObtainAzureConnection Method

        private CloudBlobClient ObtainAzureConnection(string storageAccountName, string azureKey)
        {
            var storageCredentials = new StorageCredentials(storageAccountName, azureKey);

            var cloudStorageAccount = new CloudStorageAccount(storageCredentials, true);

            return cloudStorageAccount.CreateCloudBlobClient();
        }

        #endregion

        #region ObtainContainer Method

        private async Task<CloudBlobContainer> ObtainContainer(string containerName, CloudBlobClient cloudBlobClient)
        {

            var container = cloudBlobClient.GetContainerReference(containerName);

            await container.CreateIfNotExistsAsync();

            await container.SetPermissionsAsync(
               new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

            return container;
        }

        #endregion

        #region LoadAsset Method

        public Asset LoadAsset(Guid idAsset)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region DownloadToStreamAsync

        public async Task DownloadToStreamAsync(Stream inStream)
        {
            await blobStorage.DownloadToStreamAsync(inStream);

        }

        #endregion

        #region DeleteAsset Method  

        public void DeleteAsset(Guid id)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region OverWritteAsset Method

        public Asset OverwritteAsset(Stream stream, Metadata metadata, string path, Guid assetId)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ListAssets Method

        public List<Asset> ListAssets(string path)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
