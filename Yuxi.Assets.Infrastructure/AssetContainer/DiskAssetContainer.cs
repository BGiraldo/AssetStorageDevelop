﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Yuxi.Assets.Domain.AggregatesModel.AssetAggregate;
using Yuxi.Assets.Domain.AggregatesModel.Shared;
using Yuxi.Assets.Infrastructure.AssetContainer;

namespace Yuxi.Assets.Service.Infrastructure.AssetContainer
{
    public class DiskAssetContainer : IAssetContainerService
    {
        
        private readonly string StoragePath, FileName;

        #region Constructor Method

        public DiskAssetContainer(string storagePath, string fileName, string rootPath)
        {
            StoragePath =  rootPath+storagePath;
            FileName = fileName;
        }

        #endregion

        #region CreateAsset Method

        public Asset CreateAsset(Stream inStream, Metadata metadata, string path)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region SaveAssetAsync Method

        public Task SaveAssetAsync(Stream inStream)
        {

            if (!System.IO.Directory.Exists(@StoragePath))
            {
                System.IO.Directory.CreateDirectory(@StoragePath);
            }

            int bufferSize = 1024 * 64;

            using (FileStream streamToCreate = new FileStream(@StoragePath + @"\" + FileName,
                FileMode.Create))
            {
                int bytesRead = -1;
                byte[] bytes = new byte[bufferSize];

                while ((bytesRead = inStream.Read(bytes, 0, bufferSize)) > 0)
                {
                    streamToCreate.Write(bytes, 0, bytesRead);
                    streamToCreate.Flush();
                }
            }

            return Task.Run(() => { });

        }

        #endregion

        #region DeleteAsset Method
        public void DeleteAsset(Guid id)
        {
            throw new NotImplementedException();
        }


        #endregion

        #region ListAssets

        public List<Asset> ListAssets(string path)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region LoadAsset

        public Asset LoadAsset(Guid idAsset)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region OverwritteAsset Method

        public Asset OverwritteAsset(Stream stream, Metadata metadata, string path, Guid assetId)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region DownloadToStringAsync Method

        public Task DownloadToStreamAsync(Stream inStream)
        {
            return Task.Run(() =>
            {
                FileStream file = new FileStream(StoragePath+@"\"+FileName, FileMode.Open, FileAccess.ReadWrite);
                file.CopyTo(inStream);
            });
        }

        #endregion

    }
}
