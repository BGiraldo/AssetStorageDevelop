﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Yuxi.Assets.Domain.AggregatesModel.AssetAggregate;
using Yuxi.Assets.Domain.AggregatesModel.ConsumerAggregate;
using Yuxi.Assets.Domain.AggregatesModel.Shared;

namespace Yuxi.Assets.Infrastructure
{
    public class AssetStorageContext : DbContext
    {
        const string DEFAULT_SCHEMA = "yuxi";

        public DbSet<Asset> Assets { get; set; }

        public DbSet<Consumer> Consumers { get; set; }

        public DbSet<ConsumerStoragePreference> ConsumerStoragePreferences { get; set; }

        public DbSet<ContainerType> ContainerTypes { get; set; }

        public DbSet<Metadata> Metadatas { get; set; }

        public AssetStorageContext(DbContextOptions<AssetStorageContext> options) : base(options)
        {
            base.Database.EnsureCreated();
            base.SaveChanges();
           
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ContainerType>(ConfigureContainerType);
            modelBuilder.Entity<Metadata>(ConfigureMetadata);
            modelBuilder.Entity<Consumer>(ConfigureConsumer);
            modelBuilder.Entity<ConsumerStoragePreference>(ConfigureConsumerStoragePreference);
            modelBuilder.Entity<Asset>(ConfigureAsset);

        }

        private void ConfigureAsset(EntityTypeBuilder<Asset> requestConfiguration)
        {
            requestConfiguration.ToTable("Assets", DEFAULT_SCHEMA);

            requestConfiguration.HasKey(asset => asset.Id);

            requestConfiguration
                .HasOne(asset => asset.Consumer)
                .WithMany()
                .HasForeignKey("Consumer_Id");

            requestConfiguration
               .HasOne(asset => asset.Metadata)
               .WithMany()
               .HasForeignKey("Metadata_Id");

            requestConfiguration.Property<int>("ContainerType_Id")
               .IsRequired();

            requestConfiguration.HasOne(asset => asset.ContainerType)
                .WithMany()
                .HasForeignKey("ContainerType_Id");

            requestConfiguration.Property(asset => asset.UpdatedDate)
               .IsRequired()
               .HasMaxLength(10)
               .HasColumnName("Updated_Date");

        }

        private void ConfigureConsumer(EntityTypeBuilder<Consumer> requestConfiguration)
        {

            requestConfiguration.ToTable("Consumers", DEFAULT_SCHEMA);

            requestConfiguration.HasKey(consumer => consumer.Id);

            requestConfiguration.Property(consumer => consumer.Name)
               .IsRequired()
               .HasMaxLength(60)
               .HasColumnName("Name");

            requestConfiguration.Property(consumer => consumer.Application)
               .IsRequired()
               .HasMaxLength(60)
               .HasColumnName("Application");

            requestConfiguration.Property(consumer => consumer.ContainerRootPath)
               .IsRequired()
               .HasMaxLength(200)
               .HasColumnName("Container_Root_Path");

            requestConfiguration.Property<int>("Default_Container_Id")
              .IsRequired();

            requestConfiguration.HasOne(consumer => consumer.DefaultContainer)
                .WithMany()
                .HasForeignKey("Default_Container_Id");

        }


        private void ConfigureConsumerStoragePreference(EntityTypeBuilder<ConsumerStoragePreference> requestConfiguration)
        {

            requestConfiguration.ToTable("Consumer_Storage_Preferences", DEFAULT_SCHEMA);

            requestConfiguration.HasKey(csp => csp.Id);

            requestConfiguration
               .HasOne(csp => csp.Consumer)
               .WithMany()
               .HasForeignKey("Consumer_Id");


            requestConfiguration.Property(csp => csp.MimeType)
               .IsRequired()
               .HasMaxLength(25)
               .HasColumnName("Mime_Type");

            requestConfiguration.Property<int>("Container_Id")
              .IsRequired();

            requestConfiguration.HasOne(csp => csp.Container)
                .WithMany()
                .HasForeignKey("Container_Id");

        }

        private void ConfigureContainerType(EntityTypeBuilder<ContainerType> requestConfiguration)
        {

            requestConfiguration.ToTable("Container_Types", DEFAULT_SCHEMA);

            requestConfiguration.HasKey(ct => ct.Id);

            requestConfiguration.Property(ct => ct.Id)
               .HasDefaultValue(1)
               .ValueGeneratedNever()
               .IsRequired();

            requestConfiguration.Property(ct => ct.Name)
               .IsRequired()
               .HasMaxLength(60)
               .HasColumnName("Name");

        }

        private void ConfigureMetadata(EntityTypeBuilder<Metadata> requestConfiguration)
        {

            requestConfiguration.ToTable("Metadatas", DEFAULT_SCHEMA);

            requestConfiguration.HasKey(metadata => metadata.Id);

            requestConfiguration.Property(metadata => metadata.Name)
                 .IsRequired()
                 .HasMaxLength(60)
                 .HasColumnName("Name");

            requestConfiguration.Property(metadata => metadata.MimeType)
                 .IsRequired()
                 .HasMaxLength(25)
                 .HasColumnName("Mime_Type");

            requestConfiguration.Property(metadata => metadata.SizeKb)
                 .IsRequired()
                 .HasMaxLength(25)
                 .HasColumnName("Size_kB");

            requestConfiguration.Property(metadata => metadata.ContainerRootPath)
                 .IsRequired()
                 .HasMaxLength(200)
                 .HasColumnName("Container_Root_Path");

            requestConfiguration.Property(metadata => metadata.RelativePath)
                 .IsRequired()
                 .HasMaxLength(200)
                 .HasColumnName("Relative_Path");

        }

    }



}
