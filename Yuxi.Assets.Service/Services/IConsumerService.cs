﻿

namespace Yuxi.Assets.Service.Services
{
   public interface IConsumerService
    {

       void CreateConsumer(string id, string name, string application, string containerRootPath, int defaultContainer);

       void UpdateContainerStorage(int containerTypeId, string consumerId);

    }
}
