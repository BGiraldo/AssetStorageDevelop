﻿using System;
using System.IO;
using System.Threading.Tasks;
using Yuxi.Assets.Domain.AggregatesModel.AssetAggregate;


namespace Yuxi.Assets.Service.Services
{
    public interface IAssetService
    {

        Task<Stream> DownloadAssetToStreamAsync(Guid assetId, Stream target);

        Task<Asset> CreateAssetAsync(Stream inStream, Metadata metadata,  string consumerId);

    }
}
