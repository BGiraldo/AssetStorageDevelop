﻿using System;
using System.IO;
using System.Threading.Tasks;
using Yuxi.Assets.Domain.AggregatesModel.AssetAggregate;
using Yuxi.Assets.Domain.AggregatesModel.ConsumerAggregate;
using Yuxi.Assets.Domain.AggregatesModel.Shared;
using Yuxi.Assets.Service.Factory.AssetFactory;

namespace Yuxi.Assets.Service.Services
{
    public class AssetService : IAssetService
    {

        #region Attributes

        private readonly IServiceTypeFactory _assetContainerFactory;

        private readonly IAssetRepository _assetRepository;

        private readonly IConsumerRepository _consumerRepository;

        private readonly IConsumerStoragePreferenceRepository _consumerStoragePreferencesRepository;

        #endregion

        #region Constructor

        public AssetService(IServiceTypeFactory assetContainerFactory, IAssetRepository assetRepository,
            IConsumerRepository consumerRepository, IConsumerStoragePreferenceRepository consumerStoragePreferencesRepository)
        {
            _assetContainerFactory = assetContainerFactory;
            _assetRepository = assetRepository;
            _consumerRepository = consumerRepository;
            _consumerStoragePreferencesRepository = consumerStoragePreferencesRepository;
        }

        #endregion

        #region CreateAssetAsync Method

        public async Task<Asset> CreateAssetAsync(Stream inStream, Metadata metadata, string consumerId)
        {
            var consumer = _consumerRepository.FindAsync(consumerId).Result;

            if (consumer == null)
            {
                throw new NullReferenceException("Consumer is not found");
            }

            var consumerStoragePreferences = _consumerStoragePreferencesRepository
                .ObtainConsumerStoragePreferences(consumer.Id).Result;

            ContainerType containerType = consumer.DefaultContainer;

            if (consumerStoragePreferences != null)
            {
                if (consumerStoragePreferences.MimeType.Equals(metadata.MimeType))
                {
                    containerType = consumerStoragePreferences.Container;
                }
            }

            var asset = new Asset(consumer, metadata, containerType, DateTime.Now);

            var container = _assetContainerFactory.ObtainService(asset);

            await container.SaveAssetAsync(inStream);

            _assetRepository.Add(asset);

            return asset;
        }

        #endregion

        #region DownloadAssetToStreamAsync Method

        public async Task<Stream> DownloadAssetToStreamAsync(Guid assetId, Stream target)
        {
            var assetFound = await _assetRepository.FindAsync(assetId);

            if (assetFound == null)
            {
                throw new NullReferenceException("Asset is not found");
            }

            var container = _assetContainerFactory.ObtainService(assetFound);

            await container.DownloadToStreamAsync(target);

            return target;

        }

        #endregion

    }
}
