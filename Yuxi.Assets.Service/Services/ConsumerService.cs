﻿using Yuxi.Assets.Domain.AggregatesModel.ConsumerAggregate;
using Yuxi.Assets.Domain.AggregatesModel.Shared;

namespace Yuxi.Assets.Service.Services
{
    public class ConsumerService : IConsumerService
    {
        private readonly IConsumerRepository _consumerRepository;

        public ConsumerService(IConsumerRepository consumerRepository)
        {
            _consumerRepository = consumerRepository;
        }


        public void CreateConsumer(string id, string name, string application, string containerRootPath, int defaultContainer)
        {
            ContainerType containerType = _consumerRepository.ObtainerContainerType(defaultContainer);
            Consumer consumer = new Consumer(id, name, application, containerRootPath, containerType);
            _consumerRepository.Add(consumer);
        }

        public void UpdateContainerStorage(int containerTypeId, string consumerId)
        {
            _consumerRepository.UpdateContainerStorage(containerTypeId, consumerId);
        }
    }
}
