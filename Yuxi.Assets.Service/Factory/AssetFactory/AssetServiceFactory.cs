﻿using Yuxi.Assets.Domain.AggregatesModel.AssetAggregate;
using System;
using Yuxi.Assets.Service.Infrastructure.AssetContainer;
using Yuxi.Assets.Infrastructure.AssetContainer;
using Yuxi.Assets.Domain.AggregatesModel.Shared;
using System.IO;

namespace Yuxi.Assets.Service.Factory.AssetFactory
{
    public class AssetServiceFactory : IServiceTypeFactory
    {

        private readonly IAppVariable _appVariable;

        #region Constructor Method

        public AssetServiceFactory(IAppVariable appVariable)
        {
            _appVariable = appVariable;
        }

        #endregion

        #region ObtainService Method

        public IAssetContainerService ObtainService(Asset inAsset)
        {
            int serviceType = Convert.ToInt32(inAsset.ContainerType.Id);

            var storagePath = inAsset.Metadata.ContainerRootPath
                                + @"\" + inAsset.Metadata.RelativePath;

            var fileName = inAsset.Metadata.Name;

            if (serviceType == ContainerType.Disk.Id)
            {
                return new DiskAssetContainer(storagePath, fileName, _appVariable.DiskRootPath);

            }
            else if (serviceType == ContainerType.Azure.Id)
            {
                var storageAccountName = _appVariable.StorageAccountName;

                var azureKey = _appVariable.AzureKey;

                var containerName = _appVariable.ContainerName;

                return new AzureAssetContainer(storagePath + @"\" + fileName,
                    storageAccountName, azureKey, containerName);
            }

            return new DiskAssetContainer(storagePath, fileName, _appVariable.DiskRootPath);
        }

        #endregion

    }
}
