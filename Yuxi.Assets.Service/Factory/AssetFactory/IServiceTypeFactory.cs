﻿using Yuxi.Assets.Domain.AggregatesModel.AssetAggregate;
using Yuxi.Assets.Infrastructure.AssetContainer;

namespace Yuxi.Assets.Service.Factory.AssetFactory
{
    public interface IServiceTypeFactory
    {
       IAssetContainerService ObtainService(Asset asset);
    }
}
