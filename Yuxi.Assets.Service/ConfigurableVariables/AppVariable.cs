﻿using System;
using Microsoft.Extensions.Configuration;
using Yuxi.Assets.Domain.AggregatesModel.Shared;

namespace Yuxi.Assets.Service.ConfigurableVariables
{
    public class AppVariable : IAppVariable
    {

        private readonly IConfiguration _configuration;

        public AppVariable(IConfiguration configutarion)
        {
            _configuration = configutarion;
        }
        

        public string StorageAccountName => _configuration.GetSection("AzureConfiguration")["StorageAccountName"];

        public string AzureKey => _configuration.GetSection("AzureConfiguration")["AzureKey"];

        public string ContainerName => _configuration.GetSection("AzureConfiguration")["ContainerName"];

        public string DiskRootPath => _configuration.GetSection("PathConfiguration")["DiskRootPath"];
    }
}
